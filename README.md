# Conversion Non Inclusive

## Objet

Convertir automatiquement sur le navigateur web tout texte affiché en écriture dite inclusive en texte raccourci de ces variations infinies et peu structurantes.

Le but est de faciliter la compréhension du sens du texte, et non de partir sur des considérations émotives tierces.

## Utilisation

Le filtre s'importe dans l'extension de Firefox
[FoxReplace](https://github.com/Woundorf/foxreplace) qui est installable [directement](https://addons.mozilla.org/en-US/firefox/addon/foxreplace/?src=search) depuis la rubrique des "Add-ons" de ce navigateur
